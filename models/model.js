const mongoose = require("mongoose");


// SCHEMA

var adminSchema = new mongoose.Schema({
  name: String,
  username: String,
  active : Boolean,
  email: String,
  avatar: String
})

//MODEL

module.exports = mongoose.model("Admin", adminSchema);


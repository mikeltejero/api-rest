const express = require ("express");
const app = express();
const ctrl = require('../controllers/controller')
// const ctrl

//RUTAS

//Mostrar admins
app.route('/')
.get(ctrl.getAdmins) //obtener admins
.put(ctrl.addRandomAdmin) //añadiradmin aleatorio
.post(ctrl.addAdmin) //añadir admin con el formulario
.delete(ctrl.removeAdmins) //borrar todos los admins

//Borrar admin con id
app.route("/:id")
.delete(ctrl.deleteID)
.put(ctrl.editID)

//Añadir 100 admins aleatorios
app.put('/cien', function(req,res){
	for(var i = 0; i<100; i++){
		ctrl.addRandomAdmin();
	}   
});  		  			

module.exports = app;

'use-strict'
const express = require('express');
const mongoose =  require ('mongoose')
var Schema = mongoose.Schema;
const bodyParser= require ('body-parser')
const mongodbRoute = 'mongodb://abcde:abcde@ds115625.mlab.com:15625/ejercicio-api-rest'
const app = express();
const router = require ('./routes/routes');
const port = process.env.PORT || 8080;
app.use (bodyParser.urlencoded({ extended: false}))
app.use (bodyParser.json())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
    next();
});

app.use(router);


//MONGODB

mongoose.Promise = global.Promise
mongoose.connect(mongodbRoute, (err,database) => {
    useMongoClient : true ;
    if (err) {
        return console.log(`Error al conectar a la base de datos: ${err}`)
    }
    app.listen(port, () => {
		console.log(`Servidor up en ${port}`);
	});
    console.log(`Conexión con Mongo correcta.`);
    
})


  

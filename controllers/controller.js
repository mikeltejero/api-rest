const Admin = require('../models/model')
const faker = require("faker");


//Obtener admins
exports.getAdmins = (req, res) => {
  
  Admin.find({}, function(err,perfiles){
    res.send(perfiles);
  })
  
};

//Añadir admin aleatorio
exports.addRandomAdmin = (req , res) => {
    var nuevoAdmin = new Admin({
      name: faker.name.findName(),
      username: faker.internet.userName(),
      active: faker.random.boolean(),
      email: faker.internet.email(),
      avatar: faker.image.avatar()
    })

    nuevoAdmin.save()
    .then(console.log("Admin guardado"))
    .catch(err => console.error('El Admin no ha podido guardarse: ', err));
};


//Borrar admins
exports.removeAdmins = (req, res) => {
  Admin.remove({}, function(err){
    if (!err){console.log("Admins borrados")}
  })
};

//Borrar el admin con ID 
exports.deleteID = (req, res) => {
    let adminId = req.params.id;
    console.log(adminId)
    Admin.findById(adminId, function (err,perfil){
      console.log(perfil);
      perfil.remove()
      res.status(200).send("admin borrado");
    });
};


//Añadir admin por formulario
exports.addAdmin = (req, res) => {
  new Admin({
    name: req.body.name,
    username: req.body.username,
    active: faker.random.boolean(),
    email: req.body.email,
    avatar: req.body.avatar
  }).save().then(console.log("Admin añadido"));
  res.end('{"success" : "Admin generado", "status" : 200}');
}

exports.editID = (req,res) => {
  Admin.findByIdAndUpdate(req.params.id, req.body, function ( err,perfil){
    if (err){
      console.log(err);
    }else{
      console.log(perfil);
    }
  })
};